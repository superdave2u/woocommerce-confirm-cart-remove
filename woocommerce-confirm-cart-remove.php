<?php
/*
* Plugin Name: Wocommerce Confirm Delete From Cart
* Description: Extends the shopping cart with a warning message when deleteing product.
* Author: Dave Mainville
* Author Uri: http://superdave2u.com
* 
*/
class WooConfirmCartDelete {
    function __construct() {
		if(true)
        	wp_enqueue_script( 'cart-confirm-delete', plugins_url( 'js/cart-confirm-delete.js' , __FILE__ ), 'jquery', 1, true );
    }
}
$WooConfirmCartDelete = new WooConfirmCartDelete();
?>
