jQuery(document).ready(function($){

	$('.cart-popup .delete-btn, .cart .product-remove a').unbind('click');
	
	$('.cart-popup .delete-btn, .cart .product-remove a').click(function(e){
		e.preventDefault();
		
		var $this = $(this);
		var key = $this.data('key');
		
		var c=confirm("Delete this item?");
		if (c==true) {
			$.ajax({
				method: "POST",
				url: woocommerce_params.ajax_url,
				data: {
					'action': 'et_remove_from_cart',
					'key' : key
				},
				error: function() {
					console.log('removing from cart AJAX error');
				},
				success: function(response) {
					console.log(response.msg);
					$('#cartModal').replaceWith(response.fragments.cart_modal);
					$('.shopping-cart-widget').replaceWith(response.fragments.top_cart);
					et_update_favicon();
					$this.parent().parent().remove();
					$('.success').remove();
					$('.cart').before('<p class="success">' + response.msg + '<span class="close-parent">close</span></p>');
				}
			});
		} else {
			return false;
		}
	})
});